from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
import pytest
import time
from typing import List
import re
import os

MAX_WAIT = 10


@pytest.fixture
def browser():
    browser: WebDriver = webdriver.Firefox()
    yield browser
    browser.quit()


@pytest.fixture
def wait_for_row_in_list_table():
    def _wait_for_row_in_list_table(row_text, browser):
        start_time = time.time()
        while True:
            try:
                table: WebElement = browser.find_element_by_id("id_list_table")
                rows: List[WebElement] = table.find_elements_by_tag_name("tr")
                assert row_text in [row.text for row in rows]
                return
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)

    return _wait_for_row_in_list_table


@pytest.fixture
def my_live_server(live_server):
    staging_server = os.environ.get("STAGING_SERVER")
    if staging_server:
        live_server.my_url = "http://" + staging_server
    else:
        live_server.my_url = live_server.url
    return live_server


def test_can_start_list_and_retrieve_later(
    browser, my_live_server, wait_for_row_in_list_table
) -> None:
    # user open browser and check out new web page
    browser.get(my_live_server.my_url)
    # user notices the page title and header mention to-do lists
    assert "To-Do" in browser.title
    header_text: str = browser.find_element_by_tag_name("h1").text
    assert "To-Do" in header_text
    # user can enter a to-do item straight away
    inputbox: WebElement = browser.find_element_by_id("id_new_item")
    assert inputbox.get_attribute("placeholder") == "Enter a to-do item"

    # user types "Buy milk and sugar"
    inputbox.send_keys("Buy a milk")
    #  when user hits enter, the page updates, and now:
    # "1: Buy milk and sugar"
    inputbox.send_keys(Keys.ENTER)
    wait_for_row_in_list_table("1: Buy a milk", browser)
    # there is still text box for next item
    # user writes Cook pancakes for dinner"
    inputbox1: WebElement = browser.find_element_by_id("id_new_item")
    inputbox1.send_keys("Cook pancakes for dinner")
    inputbox1.send_keys(Keys.ENTER)
    # page updates again
    wait_for_row_in_list_table("1: Buy a milk", browser)
    wait_for_row_in_list_table("2: Cook pancakes for dinner", browser)


def test_multiple_users_can_start_lists_at_diffrent_urls(
    browser, my_live_server, wait_for_row_in_list_table
):
    # Edith start a new to-do lists
    browser.get(my_live_server.my_url)
    inputbox: WebElement = browser.find_element_by_id("id_new_item")
    inputbox.send_keys("Buy a milk")
    inputbox.send_keys(Keys.ENTER)
    wait_for_row_in_list_table("1: Buy a milk", browser)

    # she notices that her list has unique url
    edith_list_url: str = browser.current_url
    print(edith_list_url)
    my_regex = re.compile(r".+/lists/.+")
    assert my_regex.match(edith_list_url)
    # new user, Francis, comes along to the site

    ## new browser session, make suer no info from cookies
    browser.quit()
    browser = webdriver.Firefox()

    # Francis visit the home page, no sign of Edith's list
    browser.get(my_live_server.my_url)
    page_text: str = browser.find_element_by_tag_name("body").text
    assert "Buy a milk" not in page_text
    assert "Cook pankaces for dinner" not in page_text

    # Francis start new list by entering a new item, he is less
    # interesting than edith
    inputboxF: WebElement = browser.find_element_by_id("id_new_item")
    inputboxF.send_keys("Buy bread")
    inputboxF.send_keys(Keys.ENTER)
    wait_for_row_in_list_table("1: Buy bread", browser)

    # Francis gets his own unique url
    francis_list_url: str = browser.current_url
    assert my_regex.match(francis_list_url)
    assert francis_list_url != edith_list_url

    # Again there is no trace of Edith list
    page_text1: str = browser.find_element_by_tag_name("body").text
    assert "Buy a milk" not in page_text1
    assert "Buy bread" in page_text1
    browser.quit()


# check css
def test_layout_and_styling(browser: WebDriver, my_live_server, wait_for_row_in_list_table):
    # go to web page
    browser.get(my_live_server.my_url)
    browser.set_window_size(1024, 768)

    # She notices the input box is nicely centered
    inputbox: WebElement = browser.find_element_by_id("id_new_item")
    assert 512 == pytest.approx(inputbox.location["x"] + inputbox.size["width"] / 2, abs=10)
    # She starts a new list and sees the input is nicely
    # centered there too
    inputbox.send_keys("testing")
    inputbox.send_keys(Keys.ENTER)
    wait_for_row_in_list_table("1: testing", browser)
    inputbox = browser.find_element_by_id("id_new_item")
    assert 512 == pytest.approx(inputbox.location["x"] + inputbox.size["width"] / 2, abs=10)

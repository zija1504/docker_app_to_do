# Provisioning a new site

## Required packages:

\* \* \* \*
nginx
Python 3.8
virtualenv + pip
Git
eg, on Ubuntu:
sudo add-apt-repository ppa:fkrull/deadsnakes
sudo apt-get install nginx git python38 python3.8-venv

## Nginx Virtual Host config

- see nginx.template.conf
- replace SITENAME with, e.g., staging.my-domain.com

## Systemd service

- see gunicorn-systemd.template.service
- replace SITENAME with, e.g., staging.my-domain.com

## Folder structure

Assume we have a user account at /home/username
/home/username\
└── sites\
└── SITENAME\
---- ├── database (if sqlite)\
---- ├── src\
---- ├── static\
---- └── virtualenv

## postgres

1. sudo apt-get update
   sudo apt-get install postgresql postgresql-contrib
2. sudo su - postgres
3. psql
4. CREATE DATABASE mydb;
5. CREATE USER myuser WITH PASSWORD 'password';
6. ALTER ROLE myuser SET client_encoding TO 'utf8';\
   ALTER ROLE myuser SET default_transaction_isolation TO 'read committed';\
   ALTER ROLE myuser SET timezone TO 'CET';
7. GRANT ALL PRIVILEGES ON DATABASE mydb TO myuser;
8. ALTER USER myuser CREATEDB;

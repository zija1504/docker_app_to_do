from fabric import task, Connection, Config
import hashlib
import time


REPO_URL = "git@github.com:zija1504/tdd.git"

hosts = ["zija@104.248.135.45"]


def _createHash():
    """This function generate 10 character long hash"""
    hash = hashlib.sha1()
    hash.update(bytearray(str(time.time()), "utf-8"))
    return hash.hexdigest()[:-10]


@task(hosts=hosts)
def create_db(c, db_name="mydb", db_user="myuser", db_password="testest"):
    c.run(f"createdb {db_name} -O {db_user}")


@task(hosts=hosts)
def deploy(c, host, db_name="mydb"):
    site_folder = f"/home/{c.user}/sites/{host}"
    source_folder = site_folder + "/src"
    c.run(f"mkdir -p {site_folder}")
    _get_latest_source(site_folder, c)
    _create_directory_structure_if_necessary(site_folder, c)
    _update_settings(source_folder, host, db_name, c)
    _update_virtualenv(site_folder, c)
    _update_static_files(site_folder, c)
    _update_database(source_folder, c)


def _create_directory_structure_if_necessary(site_folder, c) -> None:
    for subfolder in ("database", "static", "virtualenv"):
        c.run(f"mkdir -p {site_folder}/{subfolder}")


def _get_latest_source(site_folder, c) -> None:
    if c.run(f"test -d {site_folder}/.git", warn=True).failed:
        c.run(f"git clone {REPO_URL} {site_folder}")
    else:
        c.run(f"cd {site_folder} && git fetch")
    current_commit = c.local("git log -n 1 --format=%H").stdout
    c.run(f"cd {site_folder} && git reset --hard {current_commit}")


def _update_settings(source_folder, site_name, db_name, c) -> None:
    envPath = f"{source_folder}/superlists/.env"
    if not c.run(f"test -d {envPath}", warn=True).failed:
        c.run(f"rm {envPath}")
    SECRET_KEY = f"{_createHash()}"
    ALLOWED_HOSTS = site_name
    DB_NAME = db_name
    c.run(f"echo export {SECRET_KEY=} >> {envPath}")
    c.run(f"echo export DEBUG=False >> {envPath}")
    c.run(f"echo export DB_PASSWORD=testtest >> {envPath}")
    c.run(f"echo export {ALLOWED_HOSTS=} >> {envPath}")
    c.run(f"echo export {DB_NAME=} >> {envPath}")


def _update_virtualenv(site_folder: str, c) -> None:
    virtualenv_folder = site_folder + "/virtualenv"
    if c.run(f"test -d {virtualenv_folder}/bin/pip", warn=True).failed:
        c.run(f"python3.8 -m venv {virtualenv_folder}")
    c.run(f"{virtualenv_folder}/bin/pip install -r {site_folder}/requirements.txt")


def _update_static_files(site_folder, c) -> None:
    c.run(f"cd {site_folder}" " && virtualenv/bin/python src/manage.py collectstatic --noinput")


def _update_database(source_folder, c) -> None:
    c.run(
        f"cd {source_folder}"
        f" && ../virtualenv/bin/python manage.py makemigrations --noinput"
        f"&& ../virtualenv/bin/python manage.py migrate --noinput"
    )
